﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimulationController1 : MonoBehaviour
{
    // Start is called before the first frame update
    public static int healthy, recovered, sick;
    public Text labelHealthy, labelRecovered, labelSick;
    void Start()
    {
        healthy = 5;
        recovered = 0;
        sick = 1;
    }

    // Update is called once per frame
    void Update()
    {
        labelHealthy.text = healthy.ToString();
        labelRecovered.text = recovered.ToString();
        labelSick.text = sick.ToString();
    }
}
